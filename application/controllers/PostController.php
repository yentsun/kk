<?php

class PostController extends Zend_Controller_Action {

	public function init() {

		$this->mapper = new Application_Model_PostMapper;
		$this->view->headTitle()->prepend('Новости');
		$this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$this->view->title = 'Новости';
	}

	public function indexAction() {
		
		$page = $this->_getParam('page');
		$page = !empty($page) ? $page : 1;
		$this->view->posts = $this->mapper->fetchAll($page,null,4);
		$this->view->paginator = $this->mapper->paginator;
	}

	public function getAction() {
		
		$id = $this->_getParam('id');
		$this->view->post = $this->mapper->fetch($id);
		$this->view->config = Zend_Registry::get('config');	
	}
}
