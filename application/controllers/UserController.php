<?php

class UserController extends Zend_Controller_Action {

	public function init() {

		$this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$this->user = Zend_Registry::get('user');
		$this->config = Zend_Registry::get('config');
		$this->mapper = new Application_Model_UserMapper;
		$this->girlMapper = new Application_Model_GirlMapper;
		$this->mailer = new Application_Model_MailSender;
		$this->view->headScript()
			->appendFile('/js/jQuery.dPassword.min.js')
			->appendFile('/js/form.js');
		$max_upload = (int)(ini_get('upload_max_filesize'));
		$max_post = (int)(ini_get('post_max_size'));
		$memory_limit = (int)(ini_get('memory_limit'));
		$this->view->maxUpload = min($max_upload,$max_post,$memory_limit);
	}

	public function indexAction() {

		$this->view->title = 'Личный кабинет';
	}

	public function registerAction() {

		$this->view->title = 'Регистрация';
		$applicantDummy = new Application_Model_Applicant;
		$girlDummy = new Application_Model_Girl;
		$this->view->required = json_encode(array_merge($applicantDummy->getRequired(),$girlDummy->getRequired()));
		if ($this->_request->isPost()) {
			$upload = new Zend_File_Transfer_Adapter_Http();
			$config = Zend_Registry::get('config');
			$photosPath = $config->path->uploads->root.$config->path->uploads->photos;
			$count = 0;
			foreach ($upload->getFileInfo() as $fileId => $info) {
				if (stristr($info['name'],'jpg') || stristr($info['name'],'jpeg'))
					$count++;
			}
			if ($count < 3)
				$this->flashMessenger->addMessage('<div class="error">Вы предоставили менее 3 фотографий<div class="repopulate">'.htmlentities(json_encode($this->_request->getPost())).'</div></div>');
				$regResult = $this->mapper->register($this->_request->getPost());
				if (is_int($regResult) && $count >= 3) {
					$imageMapper = new Application_Model_ImageMapper;
					$i = 0;
					foreach ($upload->getFileInfo() as $info) {
						$i++;
						$pieces = explode('.',$info['name']);
						$ext = end($pieces);
						$newFileName = time().'_'.$i;
						$upload->addFilter('Rename',PUBLIC_PATH.$photosPath.$config->path->uploads->original.'/'.$newFileName.'.'.$ext,'photo'.$i);
						//	$upload->addValidator('isImage');
						if ($upload->receive('photo'.$i)) {
							$data = $this->_request->getPost();
							$data['file'] = $newFileName.'.'.$ext;
							$data['parentId'] = $regResult;
							$imageResult = $imageMapper->add($data);
							if ($imageResult instanceof Application_Model_Image) 
								$this->flashMessenger->addMessage('<div class="ok">Фото загружено</div>');
						} else {
							//$this->flashMessenger->addMessage('<div class="error">Не удалось загрузить файл :(<div class="data">'.json_encode($upload->getErrors()).'</div></div>');
						}
					}	
					$this->mailer->sendApplicantData($regResult);
					$this->flashMessenger->addMessage('<div class="ok">Ваши данные отправлены администратору. Если вас одобрят для участия в конкурсе - вы получите уведомление по почте.</div>');
				} else 
					$this->flashMessenger->addMessage($regResult);
		$this->_redirect('/user/register');
		}
	}

	public function newPasswordAction() {

		$this->view->title = 'Новый пароль';
		if ($this->_request->isPost()) {
			$data = $this->_request->getPost();
			$password = $this->mapper->accept($data,false);
			if ($password) {
				$this->mailer->sendNewPassword($data['email'],$password);
				$this->flashMessenger->addMessage('<div class="ok">Новый пароль выслан на почту '.$data['email'].'!</div>');
			} else {
				$this->flashMessenger->addMessage('<div class="error">Адреса '.$this->user->email.' нет в нашей базе!</div>');
			}
			$this->_redirect('/user/new-password');
		}
	}

	public function loginAction() {

		$this->view->title = 'Авторизация';
		if ($this->_request->isPost()) {
			if ($this->mapper->login($this->_request->getPost())) {
				$this->flashMessenger->addMessage('<div class="ok">Вы успешно авторизованы!</div>');
				$this->_redirect('/');
			} else {
				$this->flashMessenger->addMessage('<div class="error">Авторизация не удалась!</div>');
				$this->_redirect('/user/login');
			}
		}
	}

	public function logoutAction() {

		$this->mapper->logout();
		$this->_redirect('/user/login');
	}

	public function updateAction() {

		if ($this->_request->isPost()) {
			$this->flashMessenger->addMessage($this->girlMapper->update($this->_request->getPost()));
			$back = $this->_getParam('returnUrl');
			if (isset($back)) 
				$this->_redirect($back);
			else 
				$this->_redirect('/user/personal');
		}
	}

	public function personalAction() {

		$this->view->title = 'Личные данные';
		$this->view->user = $this->user;
		$this->view->girlData = $this->girlMapper->fetch($this->user->id);
		$applicantDummy = new Application_Model_Applicant;
		$girlDummy = new Application_Model_Girl;
		$this->view->required = json_encode(array_merge($applicantDummy->getRequired(),$girlDummy->getRequired()));
	}

	public function photosAction() {
	
		$this->view->headScript()
			->appendFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js')
			->appendFile('/js/jquery.form.js')
			->appendFile('/js/photos.js');
		$this->view->title = 'Мои фотографии';
		$this->view->girl = $this->girlMapper->fetch($this->user->id);
	}

	public function videosAction() {
	
		$this->view->headScript()
			->appendFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js')
			->appendFile('/js/flowplayer-3.2.6.min.js')
			->appendFile('/js/videos.js');
		$this->view->title = 'Мои видео';
		$this->view->videos = $this->girlMapper->fetch($this->user->id)->videos;
	}

	public function approveAction() {
	
		$girlId = $this->_getParam('id');
		$password = $this->mapper->accept($girlId);
		$this->mailer->sendApproval($girlId,$password);
		$this->view->girlId = $girlId;
		$this->view->title = 'Новая участница';
	}

	public function denyAction() {
	
		$girlId = $this->_getParam('id');
		$this->mapper->delete($girlId);
		$this->view->girlId = $girlId;
		$this->view->title = 'Отклонение претендентки';
		$this->flashMessenger->addMessage('<div class="notice">Участница №'.$girlId.' отклонена</div>');
		$back = $this->_getParam('returnUrl');
		if (isset($back)) 
			$this->_redirect($back);
	}
} 
