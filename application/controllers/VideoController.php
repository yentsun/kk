<?php

class VideoController extends Zend_Controller_Action {

	public function init() {
		
		$this->videoMapper = new Application_Model_VideoMapper;
		$this->user = Zend_Registry::get('user');
		$this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
	}

	public function indexAction() {
		// action body
	}

	public function addAction() {

		$userId = $this->_getParam('userid');
		if ($this->_request->isPost()) {
			$upload = new Zend_File_Transfer_Adapter_Http();
			$mimeArr = explode('/',$upload->getMimeType());
			$ext = $mimeArr[1];
			$newFileName = time();
			$config = Zend_Registry::get('config');
			$videosPath = $config->path->uploads->root.$config->path->uploads->videos;
			$upload->addFilter('Rename',PUBLIC_PATH.$videosPath.'/'.$newFileName);
			$upload->addValidator('Extension',false,'avi');
			if ($upload->isValid()) {
				$upload->receive();
				$data = $this->_request->getPost();
				$data['file'] = $newFileName;
				if ($this->user->status=='admin' && $userId)
					$data['parentId'] = $userId;
				else
					$data['parentId'] = $this->user->id;
				$this->flashMessenger->addMessage($this->videoMapper->add($data));
			} else {
				$this->flashMessenger->addMessage('<div class="error">Не удалось загрузить файл :(<div class="data">'.json_encode($upload->getErrors()).'</div></div>');
			}	
			if ($this->user->status=='admin' && $userId)
				$this->_redirect('/admin/videos/userid/'.$userId);
			else
				$this->_redirect('/user/videos');
		}
	}

	public function deleteAction() {
		
		$id = $this->_getParam('id');
		$this->videoMapper->delete($id);
		$this->flashMessenger->addMessage('<div class="ok">Видео удалено</div>');
		$this->_redirect('/user/videos');
	}
}
