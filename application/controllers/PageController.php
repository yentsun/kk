<?php

class PageController extends Zend_Controller_Action {

	public function init() {

		$this->_mapper = new Application_Model_PageMapper;
	}

	public function indexAction() {

		$slug = $this->_getParam('name');
		$page = $this->_mapper->fetch($slug);
		$this->view->page = $page;
	}

}