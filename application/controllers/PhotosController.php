<?php

class PhotosController extends Zend_Controller_Action {

	public function init() {
		
		$this->imageMapper = new Application_Model_ImageMapper;
		$this->user = Zend_Registry::get('user');
		$this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$contextSwitch = $this->_helper->getHelper('contextSwitch');
		$contextSwitch->addActionContext('add', 'xml')
			->initContext();	
	}

	public function indexAction() {
		// action body
	}

	public function addAction() {

		$userId = $this->_getParam('userid');
		if ($this->_request->isPost()) {
			$upload = new Zend_File_Transfer_Adapter_Http();
			$pieces = explode('.',$upload->getFileName());
			$ext = end($pieces);
			$newFileName = time();
			$config = Zend_Registry::get('config');
			$photosPath = $config->path->uploads->root.$config->path->uploads->photos;
			$upload->addFilter('Rename',PUBLIC_PATH.$photosPath.$config->path->uploads->original.'/'.$newFileName.'.'.$ext);
			$upload->addValidator('Extension',false,'jpeg,jpg,png');
			if ($upload->isValid()) {
				$upload->receive();
				$data = $this->_request->getPost();
				$data['file'] = $newFileName.'.'.$ext;
				if ($this->user->status=='admin' && $userId)
					$data['parentId'] = $userId;
				else
					$data['parentId'] = $this->user->id;
				$this->view->photo = $this->imageMapper->add($data);
				$this->view->status = 'ok';
			} else {
				$this->view->status = 'error';
				$this->view->errorMessage = json_encode($upload->getErrors());
			}	
		}
	}

	public function deleteAction() {
		
		$id = $this->_getParam('id');
		$this->imageMapper->delete($id);
	//	$this->flashMessenger->addMessage('<div class="ok">Фото удалено</div>');
//		$this->_redirect('/user/photos');
	}
}
