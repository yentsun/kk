<?php

class GalleryController extends Zend_Controller_Action {

	public function init() {

		$this->mapper = new Application_Model_GalleryItemMapper;
		$this->view->title = 'Галерея участниц';
		$this->view->headScript()
			->appendFile('/js/gallery.js')
		;
	}

	public function indexAction() {

		$page = $this->_getParam('page');
		$page = !empty($page) ? $page : 1;
		$this->view->items = $this->mapper->fetchAll($page);
		$this->view->paginator = $this->mapper->paginator;
		$this->view->historyMapper = new Application_Model_HistoryMapper;
	}
}
