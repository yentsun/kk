<?php

class IndexController extends Zend_Controller_Action {

	public function init() {
		
		$this->galleryMapper = new Application_Model_GalleryItemMapper;
		$this->postMapper = new Application_Model_PostMapper;
		$this->view->headScript()
			->appendFile('/js/gallery.js')
		;
	}

	public function indexAction() {
    
    		$this->view->bodyId = 'index';
		$this->view->top4 = $this->galleryMapper->fetchAll(null,null,4);
		$this->view->top5 = $this->galleryMapper->fetchAll(null,true,5);
		$this->view->posts = $this->postMapper->fetchAll(null,4);
	}
}
