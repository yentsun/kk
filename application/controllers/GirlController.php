<?php

// = Showcase

class GirlController extends Zend_Controller_Action {

	public function init() {
		
		$this->mapper = new Application_Model_GirlMapper;
		$this->userMapper = new Application_Model_UserMapper;
		$this->historyMapper = new Application_Model_HistoryMapper;
	}

	public function indexAction() {

		$this->view->title = 'Участница';
		$girlId = $this->_getParam('id');
		$this->view->girl = $this->mapper->fetch($girlId);
		$this->view->girlUser = $this->userMapper->fetch($girlId);
		$this->view->voted = $this->historyMapper->recordPresent('votedFor',$girlId);
    		$this->_helper->layout->disableLayout();
	}

	public function voteAction() {
		
		if ($this->_request->isPost()) {
			$data = $this->_request->getPost();
			if (!$this->historyMapper->recordPresent('votedFor',$data['id'])) {
				$this->mapper->submitVote($data);
				$this->historyMapper->addItem('votedFor',$data['id']);			
			}
		}
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}
	
	public function selectAction() {
		
		if ($this->_request->isPost()) {
			$data = $this->_request->getPost();
			$this->mapper->toggleSelected($data);
		}
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}
}
