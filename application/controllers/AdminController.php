<?php

class AdminController extends Zend_Controller_Action {

	public function init() {
		
		$userId = $this->_getParam('userid');
		if ($userId) {
			$userMapper = new Application_Model_UserMapper;
			$this->view->user = $userMapper->fetch($userId);
		}
		$this->galleryMapper = new Application_Model_GalleryItemMapper;
		$this->postMapper = new Application_Model_PostMapper;
		$this->pageMapper = new Application_Model_PageMapper;
		$this->girlMapper = new Application_Model_GirlMapper;
		$this->view->adminMode = true;
		$this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$this->view->headScript()
			->appendFile('/js/admin.js')
			->appendFile('/js/form.js')
			->appendFile('/js/ckeditor/ckeditor.js')
			->appendFile('/js/ckeditor/adapters/jquery.js')
			;
	}

	public function indexAction() {

		$sort = $this->_getParam('sort');
		$this->view->title = 'Администрирование';
		$this->view->girls = $this->galleryMapper->fetchAll(false,true,false,$sort,null);
	}
	
	public function createPostAction() {
		
		if ($this->_request->isPost()) {
			$data = $this->_request->getpost();
			$this->flashMessenger->addMessage($this->postMapper->create($data));
			$this->_redirect('/admin/posts');
		}
		$this->view->title = 'Создание новости';
		$dummy = new Application_Model_Post;
		$this->view->required = json_encode(array_merge($dummy->getRequired()));
	}

	public function postsAction() {
		
		$this->view->title = 'Редактирование новостей';
		$this->view->posts = $this->postMapper->fetchAll();
	}

	public function pagesAction() {
		
		$this->view->title = 'Редактирование страниц';
		$this->view->pages = $this->pageMapper->fetchAll();
	}

	public function updatePostAction() {
		
		$this->view->title = 'Редактирование новости';
		$this->view->post = $this->postMapper->fetch($this->_getParam('id'));
		$dummy = new Application_Model_Post;
		$this->view->required = json_encode(array_merge($dummy->getRequired()));
		if ($this->_request->isPost()) {
			$data = $this->_request->getpost();
			$this->flashMessenger->addMessage($this->postMapper->update($data));
			$this->_redirect('/admin/update-post/id/'.$data['id']);
		}
	}

	public function updatePageAction() {
		
		if ($this->_request->isPost()) {
			$data = $this->_request->getpost();
			$this->flashMessenger->addMessage($this->pageMapper->update($data));
			$this->_redirect('/admin/update-page/id/'.$data['slug']);
		}
		$this->view->title = 'Редактирование страницы';
		$this->view->page = $this->pageMapper->fetch($this->_getParam('id'));
		$dummy = new Application_Model_Page;
		$this->view->required = json_encode(array_merge($dummy->getRequired()));
	}

	public function deletePostAction() {
		
		$id = $this->_getParam('id');
		$this->flashMessenger->addMessage($this->postMapper->delete($id));
		$this->_redirect('/admin/posts');
	}

	public function personalAction() {

		$this->view->title = 'Личные данные участницы';
		$this->view->girlData = $this->girlMapper->fetch($this->view->user->id);
		$this->view->back = '/admin';
		$applicantDummy = new Application_Model_Applicant;
		$girlDummy = new Application_Model_Girl;
		$this->view->required = json_encode(array_merge($applicantDummy->getRequired(),$girlDummy->getRequired()));
		$this->renderScript('user/personal.phtml');
	}

	public function photosAction() {
	
		$this->view->title = 'Фотографии участницы';
		$this->view->girl = $this->girlMapper->fetch($this->view->user->id);
		$this->view->headScript()
			->appendFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js')
			->appendFile('/js/jquery.form.js')
			->appendFile('/js/photos.js');
		$this->renderScript('user/photos.phtml');
	}

	public function videosAction() {
	
		$this->view->title = 'Видео участницы';
		$this->view->videos = $this->girlMapper->fetch($this->view->user->id)->videos;
		$max_upload = (int)(ini_get('upload_max_filesize'));
		$max_post = (int)(ini_get('post_max_size'));
		$memory_limit = (int)(ini_get('memory_limit'));
		$this->view->maxUpload = min($max_upload,$max_post,$memory_limit);
		$this->view->headScript()
			->appendFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js')
			->appendFile('/js/flowplayer-3.2.6.min.js')
			->appendFile('/js/videos.js');
		$this->renderScript('user/videos.phtml');
	}
}
