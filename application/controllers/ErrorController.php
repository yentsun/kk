<?php

class ErrorController extends Zend_Controller_Action
{

    public function errorAction() {

        $errors = $this->_getParam('error_handler');
	$this->view->title = 'Ошибка!';
        
        if (!$errors) {
            $this->view->message = 'You have reached the error page';
            return;
        }
        
	switch ($errors->type) {
		case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
		case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
		case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

			// 404 error -- controller or action not found
			$this->getResponse()->setHttpResponseCode(404);
			$this->view->message = 'Страница не найдена (404)';
			break;
		default:
			$this->getResponse()->setHttpResponseCode(500);
			$this->view->message = 'Ошибка приложения (см. лог)';
			if ($log = $this->getLog()) 
				$log->err($errors->exception);
			break;
	}
        
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        
        $this->view->request = $errors->request;
    }

	public function unauthorizedAction() {
		
		$this->view->title = 'Доступ запрещен';		
	}

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}

