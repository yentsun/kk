<?php

return array(
    Zend_Filter_Input::NOT_EMPTY_MESSAGE => "обязательно нужно заполнить",
    "'%value%' does not appear to be an integer" => "поле должно содержать число",
    "'%value%' is no valid email address in the basic format local-part@hostname" => "'%value%' - неверный адрес электронной почты",
    "A record matching '%value%' was found" => "пользователь '%value%' уже зарегистрирован",
    "'%value%' is less than %min% characters long" => "Поле должно содержать от %min% символов",
    "'%value%' does not fit the date format '%format%'" => "'%value%' - неверный формат даты",
    "'%value%' must contain only digits" => "поле должно содержать только цифры"
);