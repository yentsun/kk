<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

	protected function _initConfig() {
		$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/settings.ini',APPLICATION_ENV);
		Zend_Registry::set('config', $config);
	}

	protected function _initLog() {

		if ($this->hasPluginResource('log')) {
      			$r = $this->getPluginResource('log');
      			$log = $r->getLog();
      			Zend_Registry::set('log', $log);
    		}
  	}
}
