<?php

class Application_Form_UploadPhoto extends Zend_Form {

	public function __construct($id,$extensions='jpg,png,gif',$options=null) {

		parent::__construct($options);

		$file = new Zend_Form_Element_File('file');
		$max_upload = (int) (ini_get('upload_max_filesize'));
		$file
				->setLabel('Выберите файл ('.$extensions.', не более '.$max_upload.'Мб):')
				->setRequired(true)
				->addValidator('Count',false,1)
				->addValidator('Size',false,$max_upload * 1000000)
				->addValidator('Extension',false,$extensions)
		;

		$description = new Zend_Form_Element_Textarea('description');
		$formId = new Zend_Form_Element_Hidden($id);
		$submit = new Zend_Form_Element_Submit('Загрузить');

		$this
				->setAttrib('enctype','multipart/form-data')
				->setAttrib('id',$id)
		;
		if($id == 'addphoto')
			$this->addElements(array($formId,$file,$description,$submit));
		if($id == 'addflash') {
			$this
					->addElements(array($formId,$file,$submit))
					->setAction('/uploadflash')
			;
		}
	}

}

