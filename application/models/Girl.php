<?php

class Application_Model_Girl extends Whyte_Model_Entity {

	protected function _dataPattern() {

		return array(
		    'id'		=> array(array('GreaterThan',1),'allowEmpty' => 'true'),
		    'birthDate' 	=> array(array('Date','YYYY-MM-dd')),
		    'privateInfo'	=> array('allowEmpty' => true),
		    'phone'		=> array('Digits','allowEmpty'=>true),
		    'height'		=> array('Int','allowEmpty'=>true),
		    'weight'		=> array('Int','allowEmpty'=>true),
		    'measures'		=> array('allowEmpty'=>true), // 90-60-90
		    'hair'		=> array('allowEmpty'=>true),
		    'city'		=> array('allowEmpty'=>true),
		    'rating'		=> array('Float','allowEmpty'=>true),
		    'votes'		=> array('Int','allowEmpty'=>true),
		    'selected'		=> array('Digits','allowEmpty'=>true),
		    'photos'		=> array('allowEmpty'=>true),
		    'privatePhotos'		=> array('allowEmpty'=>true),
		    'videos'		=> array('allowEmpty'=>true)
		);
	}

	protected function _postDataPopulation() {
		
		$this->_attachPhotos();
		$this->_attachVideos();
	}
	
	private function _attachPhotos() {
		
		$mapper = new Application_Model_ImageMapper;
		if ($this->id) {
			$this->photos = $mapper->fetchByGirlId($this->id);
			$this->privatePhotos = $mapper->fetchByGirlId($this->id,2);
		}
	}

	private function _attachVideos() {
		
		$mapper = new Application_Model_VideoMapper;
		if ($this->id)
			$this->videos = $mapper->fetchById($this->id);
	}

	public function age() {
		$today = new Zend_Date();
		$birthDate = new Zend_Date($this->birthDate);
		return $today->toString('Y') - $birthDate->toString('Y');
	}
}
