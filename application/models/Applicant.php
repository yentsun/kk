<?php

class Application_Model_Applicant extends Whyte_Model_Entity {

	protected function _dataPattern() {

		$mapper = new Application_Model_UserMapper;
		$map = $mapper->getMap();
		$girlDummy = new Application_Model_Girl;

		return array(
		    'email' => array('EmailAddress',new Zend_Validate_Db_NoRecordExists($mapper->getTableName(),$map['email']),'presence' => 'required'),
		    'password' => array(array('StringLength',4),'allowEmpty'=>true),
		    'firstName' => array(),
		    'lastName' => array(),
		    'birthDate' => $girlDummy->getValidators('birthDate')
		);
	}

	public function prepare() {

		$data = $this->toArray();
		//$data['registrationDate'] = Zend_Date::now()->toString('YYYY-MM-dd');
		if (!isset($data['password']))
			$data['password'] = time();
		$salt = null;
		for ($i = 0; $i < 50; $i++) $salt .= chr(rand(33,126));
		$data['salt'] = $salt;
		$data['digest'] = md5($data['salt'].$data['password']);
		unset($data['password']);
		return $data;
	}
}
