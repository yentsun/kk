<?php

class Application_Model_GirlMapper extends Whyte_Model_Mapper {

	protected $_tableName = 'girls_data';
	protected $_entityClass = 'Application_Model_Girl';
	protected $_mappers = array(
		'User'
	);

	protected function _setMap() {

		$this->_map = array(
		    'id'		=> 'user_id',
		    'birthDate' 	=> 'girl_birthdate',
		    'privateInfo'	=> 'girl_private_info',
		    'phone'		=> 'girl_phone',
		    'height'		=> 'girl_height',
		    'weight'		=> 'girl_weight',
		    'measures'		=> 'girl_measures',
		    'hair'		=> 'girl_hair',
		    'city'		=> 'girl_city',
		    'rating'		=> 'girl_rating',
		    'votes'		=> 'girl_votes',
		    'selected'		=> 'girl_selected'
		);
	}

	public function register(Application_Model_Girl $girl) {
		
		return $this->_gateway->insert($this->_toMappedArray($girl));
	}

	public function fetch($userId) {
	
		$select = $this->_gateway->select()
				->where($this->_tableName.'.'.$this->_map['id'].' = ?',$userId)
		;
		$row = $this->_gateway->fetchRow($select);
		return $this->_factory($row);
	}

	public function update(array $data) {

		$userUpdate = $this->_mappers['User']->update($data,$data['id']);
		if (!stristr($userUpdate,'ok'))
			return $userUpdate;
		$updatedData = new Application_Model_Girl($data);
		if ($updatedData->hasErrors())
			return $this->_errorAndRepopulate($updatedData->getErrors(),$data);
		$girlData = $this->_toMappedArray($updatedData);
		$this->getGateway()->update($girlData,array($this->_map['id'].' = ?' => $updatedData->id));
		return '<div class="ok">Данные обновлены!</div>';
	}

	public function submitVote(array $data) {
	
		$girl = $this->fetch($data['id']);
		$girl->rating = $girl->rating + $data['rating'];
		$girl->votes = $girl->votes+1;
		$this->update($girl->toArray());
	}

	public function toggleSelected(array $data) { //зд. select = выбрать для финала
	
		$girl = $this->fetch($data['id']);
		$girl->selected = (int) $girl->selected ? 0 : 1;
		$this->update($girl->toArray());
	}
}
