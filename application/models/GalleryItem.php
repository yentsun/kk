<?php

class Application_Model_GalleryItem extends Whyte_Model_Entity {

	protected function _dataPattern() {

		return array(
		    'userId' => array('Int'),
		    'name' => array(),
		    'lastName' => array(),
		    'image' => array('allowEmpty' => true),
		    'rating' => array('Int'),
		    'regDate' => array('Date'),
		    'birthDate' => array('Date'),
		    'height' => array('Int'),
		    'weight' => array('Int'),
		    'measures' => array(),
		    'selected' => array()
		);
	}

	public function age() {
		$today = new Zend_Date();
		$birthDate = new Zend_Date($this->birthDate);
		return $today->toString('Y') - $birthDate->toString('Y');
	}
}
