<?php

require_once 'WideImage/WideImage.php';
require_once 'Image.php';

class Application_Model_Image extends Whyte_Model_Entity {
	
	private $_config = array();

	protected function _dataPattern() {

		return array(
		    'id' => array('Int','allowEmpty' => true),
		    'parentId' => array('Int'),
		    'file' => array('presence' => 'required'),
		    'status' => array('Int','allowEmpty' => true),
		    'description' => array('allowEmpty' => true)
		);
	}
	
	protected function _postDataPopulation() {
		
		$config = Zend_Registry::get('config');
		$photosPath = $config->path->uploads->root.$config->path->uploads->photos;
		$pieces = explode('.',$this->file);
		$this->_config['thumb'] = array(
			'path'	=>	$photosPath.$config->path->uploads->thumb.'/'.$this->file,
			'size'	=>	$config->size->thumb
		);
		$this->_config['big'] = array(
			'path'	=>	$photosPath.$config->path->uploads->big.'/'.$this->file,
			'size'	=>	$config->size->big
		);
		$this->_config['original'] = array(
			'path'	=>	$photosPath.$config->path->uploads->original.'/'.$this->file
		);
		$this->_config['portrait'] = array(
			'path'	=>	$photosPath.'/portrait/'.$pieces[0].'.png'
		);
		$this->_config['title'] = array(
			'path'	=>	$photosPath.'/title/'.$pieces[0].'.png'
		);
	}

	public function variant($variant) {

		if (!$this->_variantExists($variant)) {
			if (!$this->hasErrors()) 
				$this->_createVariant($variant);
			else {
				$logger = Zend_Registry::get('log');
				$logger->log('Image has errors: '.json_encode($this->getErrors()),Zend_Log::WARN);
			}
		}
		return $this->_config[$variant]['path'];
	}

	private function _variantExists($variant) {
		
		if (file_exists(PUBLIC_PATH.$this->_config[$variant]['path'])) 
			return true;
		else 
			return false;
	}
	
	private function _createVariant($variant) {
	
		if ($variant!='big')
			$source = PUBLIC_PATH.$this->variant('big');
		$destination = PUBLIC_PATH.$this->_config[$variant]['path'];

		switch ($variant) {
			case 'thumb':
				copy($source,$destination);
				$image = new Image($destination);
				$image->resize($this->_config[$variant]['size'],true,true);
				$image->save();
				break;
			case 'big':
				copy(PUBLIC_PATH.$this->_config['original']['path'],$destination);
				$image = new Image($destination);
				$image->resize($this->_config[$variant]['size']);
				$image->save();
				unlink(PUBLIC_PATH.$this->_config['original']['path']);
				break;
			case 'portrait':
				$image = WideImage::load($source);
				$cropped = $image
					->resize(109,null,'inside')
					->crop(0,0,'100%',109)
					->unsharp(50,1,1)
					;
				$bg = WideImage::load(PUBLIC_PATH.'/img/portrait-bg.png');
				$final = $bg
					->merge($cropped,18,13)
					;
				$final->saveToFile(PUBLIC_PATH.$this->_config['portrait']['path']);
				break;
			case 'title':
				$image = WideImage::load($source);
				$cropped = $image
					->resize(181,null,'inside')
					->crop(0,0,'100%',244)
					->unsharp(50,1,1)
					;
				$bg = WideImage::load(PUBLIC_PATH.'/img/title-portrait-bg.png');
				$final = $bg
					->merge($cropped,12,5)
					;
				$final->saveToFile(PUBLIC_PATH.$this->_config['title']['path']);
				break;
		}
		//$logger->log('Image created ('.$variant.' - '.memory_get_peak_usage().')!', Zend_Log::WARN);
	}
	
	private function _removeVariant($variant) {
		
		if ($this->_variantExists($variant))
			unlink(PUBLIC_PATH.$this->_config[$variant]['path']);
	}

	public function wipe() {
	
		foreach ($this->_config as $variant => $settings) {
			$this->_removeVariant($variant);
		}
	}
}
