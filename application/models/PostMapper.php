<?php

class Application_Model_PostMapper extends Whyte_Model_Mapper {

	protected $_tableName = 'posts';
	protected $_entityClass = 'Application_Model_Post';

	protected function _setMap() {

		$this->_map = array(
		    'id' => 'post_id',
		    'title' => 'post_title',
		    'body' => 'post_body',
		    'createdAt' => 'post_created_at'
		);
	}

	public function fetch($id) {

		$select = $this->_gateway
			->select()
			->where($this->_map['id'].' = ?',$id)
		;
		$row = $this->_gateway->fetchRow($select);
		return $this->_factory($row);
	}

	public function fetchAll($page=null,$limit=null,$perPage=null) {
		
		$select = $this->_gateway
			->select()
			->order($this->_map['createdAt'].' DESC')
		;
		if ($limit) $select->limit($limit);
		//if ($catId)
		//	$select->where($this->_tableName.'.'.$this->_map['category'].' = ?',$catId);
		if (!$page)
			$rows = $this->_gateway->fetchAll($select);
		else
			$rows = $this->_paginate($select,$page,$perPage);
		return $this->_prepareItems($rows);

	}

	public function create(array $data) {
		
		$newPost = new $this->_entityClass($data);
		if ($newPost->hasErrors())
			return $this->_errorAndRepopulate($newPost->getErrors(),$data);
		else {
			$this->_gateway->insert($this->_toMappedArray($newPost));
			return '<div class="ok">Новость успешно опубликована.</div>';
		}
	}

	public function update(array $data) {
		
		$updatedPost = new $this->_entityClass($data);
		if ($updatedPost->hasErrors())
			return $this->_errorAndRepopulate($updatedPost->getErrors(),$data);
		else {
			$this->_gateway->update($this->_toMappedArray($updatedPost),array($this->_map['id'].' = ?' => $updatedPost->id));
			return '<div class="ok">Новость успешно обновлена.</div>';
		}
	}

	public function delete($id) {
		
		$this->_gateway->delete($this->_map['id'].' = '.$id);
		return '<div class="notice">Новость удалена</div>';
	}
}
