<?php

class Application_Model_GalleryItemMapper extends Whyte_Model_Mapper {

	protected $_tableName = 'girls_data';
	protected $_entityClass = 'Application_Model_GalleryItem';
	protected $_mappers = array(
	    'User',
	    'Image'
	);

	protected function _setMap() {

		$this->_map = array(
		    'userId' => 'user_id',
		    'name' => 'user_fname',
		    'lastName' => 'user_lname',
		    'rating' => 'girl_rating',
		    'image' => 'photo_file',
		    'regDate' => 'user_registered_at',
		    'birthDate' => 'girl_birthdate',
		    'height' => 'girl_height',
		    'weight' => 'girl_weight',
		    'measures' => 'girl_measures',
		    'status' => 'user_status',
		    'selected' => 'girl_selected'
		);
	}

	public function fetchAll($page=false,$acceptedOnly=true,$limit=false,$sort=null,$status=1) {

		$select = $this->getGateway()
				->select()
				->setIntegrityCheck(false)
				->from($this->getGateway())
				->group($this->_tableName.'.'.$this->_map['userId'])
		;
		if ($acceptedOnly) 
			$select->where($this->_map['status'].' = ?','model');
		if ($status)
			$select->where($this->_mappers['Image']->getMapValue('status').' = ?',$status);
		if ($limit) 
			$select->limit($limit);
		if ($sort) {
			if (in_array($sort,array('name','lastName')))
				$direction = 'ASC';
			else 
				$direction = 'DESC';
			$select->order($this->_map[$sort].' '.$direction);
		}
		else {	
			$select
				->order($this->_map['selected'].' DESC')
				->order($this->_map['rating'].' DESC')
			;
		}
		$select = $this->_addJoins($select);
		if ($page)
			$rows = $this->_paginate($select,$page);
		else
			$rows = $this->getGateway()->fetchAll($select);
		return $this->_prepareItems($rows);
	}

	private function _addJoins(Zend_Db_Select $select) {
	
		$select = $this->_mappers['Image']->addJoin($select);
		$select = $this->_mappers['User']->addJoin($select);
		return $select;
	}

	protected function _prepareItems($rows) {
		
		$items = array();
		foreach ($rows as $row) {
			$itemObject = $this->_factory($row);
			$itemObject->image = new Application_Model_Image(array('file'=>$itemObject->image,'parentId'=>$itemObject->userId));
			$items[] = $itemObject;
		}
		return $items;
	}
}
