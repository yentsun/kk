<?php

class Application_Model_VideoMapper extends Whyte_Model_Mapper {

	protected $_tableName = 'videos';
	protected $_entityClass = 'Application_Model_Video';
	protected $_joinBy = 'user_id';

	protected function _setMap() {

		$this->_map = array(
			'id'		=> 'video_id',
		    	'parentId'	=> 'user_id',
		    	'file' 		=> 'video_file',
		    	'description'	=> 'video_description',
		);
	}

	// получить видео по id пользователя

	public function fetchById($userId) {
	
		$select = $this->_gateway
			->select()
			->where($this->_map['parentId'].' = ?',$userId);
		$rows = $this->_gateway->fetchAll($select);
		$items = array();
		foreach ($rows as $row) {
			$items[] = $this->_factory($row);
		}
		return $items;
	}

	// получить видео по его id

	public function fetch($videoId) {
	
		$select = $this->_gateway
			->select()
			->where($this->_map['id'].' = ?',$videoId);
		$row = $this->_gateway->fetchRow($select);
		return $this->_factory($row);
	}

	// добавление видео

	public function add($data) {
		
		$video = new $this->_entityClass($data);
		if ($video->hasErrors())
			return $this->_errorAndRepopulate($video->getErrors(),$data);
		$video->convert();
		$this->_gateway->insert($this->_toMappedArray($video));
		return '<div class="ok">Видео загружено</div>';
	}

	// удаление видео по его id

	public function delete($videoId) {
		
		$video = $this->fetch($videoId);
		$video->wipe();
		$this->_gateway->delete($this->_map['id'].' = '.$videoId);
	}
}
