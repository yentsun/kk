<?php

class Application_Model_Post extends Whyte_Model_Entity {

	protected function _dataPattern() {

		return array(
		    'id' => array('Int','allowEmpty' => true),
		    'title' => array('presence' => 'required'),
		    'body' => array('allowEmpty' => true),
		    'createdAt' => array(array('Date','YYYY-MM-dd'),'allowEmpty' => true)
		);
	}
	
	public function intro($wordCount=40) {
		
		return implode(' ',(array_slice(explode(' ',strip_tags($this->body), $wordCount+1),0,$wordCount)));
	}

	public function date($format) {
		
		$this->createdAt = new Zend_Date($this->createdAt);
		return $this->createdAt->toString($format);
	}
}
