<?php

class Application_Model_Page extends Whyte_Model_Entity {

	protected function _dataPattern() {

		return array(
		    'slug' => array('Alnum','presence' => 'required'),
		    'title' => array('presence' => 'required'),
		    'body' => array('allowEmpty' => true),
		    'type' => array('presence' => 'required')
		);
	}
}
