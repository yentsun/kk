<?php

class Application_Model_UserMapper extends Whyte_Model_Mapper {

	protected $_tableName = 'users';
	protected $_entityClass = 'Application_Model_User';

	protected function _setMap() {

		$this->_map = array(
		    'id' => 'user_id',
		    'email' => 'user_email',
		    'salt' => 'user_salt',
		    'digest' => 'user_digest',
		    'firstName' => 'user_fname',
		    'lastName' => 'user_lname',
		    'registrationDate' => 'user_registered_at',
		    'status' => 'user_status'
		);
	}

	public function fetch($id) {
		
		$select = $this->_gateway->select()
				->where($this->_tableName.'.'.$this->_map['id'].' = ?',$id)
				->orWhere($this->_tableName.'.'.$this->_map['email'].' = ?',$id)
		;
		$row = $this->_gateway->fetchRow($select);
		return $this->_factory($row);
	}

	public function getUser() {

		$auth = Zend_Auth::getInstance();
		if ($auth->hasIdentity()) {
			$identity = $auth->getIdentity();
			$map = $this->getMap();
			$gateway = $this->getGateway();
			$select = $gateway->select()
					->where($map['email'].' = ?',$identity)
			;
			$row = $gateway->fetchRow($select);
			return $this->_factory($row);
		} else
			return null;
	}

	public function register(array $data) { 

		$applicant = new Application_Model_Applicant($data);
		$girlData = new Application_Model_Girl($data);
		if ($applicant->hasErrors())
			return $this->_errorAndRepopulate($applicant->getErrors(),$data);
		if ($girlData->hasErrors())
			return $this->_errorAndRepopulate($girlData->getErrors(),$data);
		$readyApplicant = $applicant->prepare();
		$readyApplicant->status = 'applicant';
		$lastId = $this->getGateway()->insert($this->_toMappedArray($readyApplicant));
		$girlMapper = new Application_Model_GirlMapper;
		$girlData->id = $lastId;
		if ($girlMapper->register($girlData))
			return (int) $lastId;
	}

	public function accept($userid,$model=true) { //прием нового пользователя/новый пароль
		
		$user = $this->fetch($userid);
		if ($user) {
			if ($model) 
				$user->status = 'model';
			$newPass = $this->_generatePassword();
			$user->digest($newPass);
			$this->_gateway->update($this->_toMappedArray($user),array($this->_map['id'].' = ?' => $user->id));
			return $newPass;
		} else
			return false;
	}

	public function update(array $data,$userId=null) {

		$user = !$userId ? $this->getUser() : $this->fetch($userId);
		$updatedUser = $user->update($data);
		if ($updatedUser->hasErrors())
			return $this->_errorAndRepopulate($updatedUser->getErrors(),$data);
		$this->getGateway()->update($this->_toMappedArray($updatedUser),array($this->_map['id'].' = ?' => $updatedUser->id));
		return '<div class="ok">Данные обновлены!</div>';
	}

	public function delete($userid) {
		
		$where = $this->_gateway->getAdapter()->quoteInto($this->_map['id'].' = ?',$userid);
		$this->_gateway->delete($where);
		//TODO дописать удаление фотографий
	}

	public function login($data) {

		$authAdapter = $this->_getAuthAdapter($data);
		$auth = Zend_Auth::getInstance();
		$result = $auth->authenticate($authAdapter);
		if ($result->isValid()) {
			Zend_Session::rememberMe(3600 * 24 * 365);
			return true;
		} else
			return false;
	}

	public function logout() {

		$auth = Zend_Auth::getInstance();
		Zend_Session::forgetMe();
		$auth->clearIdentity();
	}

	private function _getAuthAdapter($incomingData) {

		$map = $this->getMap();
		$authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
		$authAdapter
			->setTableName($this->_tableName)
			->setIdentityColumn($map['email'])
			->setCredentialColumn($map['digest'])
			->setCredentialTreatment('md5(concat('.$map['salt'].',?))');
		$authAdapter->getDbSelect()->orWhere($map['email'].'= ?',$incomingData['login_email']);
		$authAdapter->setIdentity($incomingData['login_email']);
		$authAdapter->setCredential($incomingData['password']);
		return $authAdapter;
	}

	private function _generatePassword() {
		
		// оригинал: http://www.zend.com//code/codex.php?ozid=149&single=1 
		// set password length
		$pw_length = 15;
		// set ASCII range for random character generation
		$lower_ascii_bound = 50;          // "2"
		$upper_ascii_bound = 122;       // "z"
        	// Exclude special characters and some confusing alphanumerics
        	// o,O,0,I,1,l etc
        	$notuse = array (58,59,60,61,62,63,64,73,79,91,92,93,94,95,96,108,111);
		$i = 0;
		$password = null;
        	while ($i < $pw_length) {
                	mt_srand ((double)microtime() * 1000000);
                	// random limits within ASCII table
                	$randnum = mt_rand ($lower_ascii_bound, $upper_ascii_bound);
                	if (!in_array ($randnum, $notuse)) {
                        	$password = $password . chr($randnum);
                        	$i++;
                	}
        	}
		return $password;
	}
}
