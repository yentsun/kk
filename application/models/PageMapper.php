<?php

class Application_Model_PageMapper extends Whyte_Model_Mapper {

	protected $_tableName = 'pages';
	protected $_entityClass = 'Application_Model_Page';

	protected function _setMap() {

		$this->_map = array(
		    'slug' => 'page_slug',
		    'title' => 'page_title',
		    'body' => 'page_body',
		    'type' => 'page_type'
		);
	}

	public function fetch($slug) {

		$select = $this->_gateway->select()
				->where($this->_map['slug'].' = ?',$slug)
		;
		$row = $this->_gateway->fetchRow($select);
		$page = $this->_factory($row);
		return $page;
	}

	public function fetchAll() {
		
		$select = $this->_gateway
			->select()
		;
		$rows = $this->_gateway->fetchAll($select);
		return $this->_prepareItems($rows);

	}

	public function fetchBlocks() {
		
		$select = $this->_gateway
			->select()
			->where($this->_map['type'].' = ?','block')
		;
		$rows = $this->_gateway->fetchAll($select);
		$result = array();
		foreach ($rows as $row) {
			$block = $this->_factory($row);
			$result[$block->slug] = $block->body;
		}
		return $result;
	}

	public function update(array $data) {
		
		$page = $this->fetch($data['slug']);
		$updatedPage = $page->update($data);
		if ($updatedPage->hasErrors())
			return $this->_errorAndRepopulate($updatedPage->getErrors(),$data);
		else {
			$this->_gateway->update($this->_toMappedArray($updatedPage),array($this->_map['slug'].' = ?' => $updatedPage->slug));
			return '<div class="ok">Страница успешно обновлена.</div>';
		}
	}
}
