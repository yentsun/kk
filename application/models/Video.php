<?php

class Application_Model_Video extends Whyte_Model_Entity {
	
	private $_config = array();

	protected function _dataPattern() {

		return array(
		    'id' => array('Int','allowEmpty' => true),
		    'parentId' => array('Int'),
		    'file' => array('presence' => 'required'),
		    'description' => array('allowEmpty' => true)
		);
	}
	
	protected function _postDataPopulation() {
		
		$config = Zend_Registry::get('config');
		$videosPath = $config->path->uploads->root.$config->path->uploads->videos;
		$pieces = explode('.',$this->file);
		$this->_config = array(
			'path'	=>	$videosPath.'/',
			'name'	=> 	$pieces[0],
			'ext'	=>	$pieces[1]
		);
	}

	public function convert($format='flv') {

		$presentFile = PUBLIC_PATH.$this->_config['path'].$this->file;
		$neededFile = PUBLIC_PATH.$this->_config['path'].$this->_config['name'].'.'.$format;
		if (!file_exists($neededFile)) {
			exec('ffmpeg -i '.$presentFile.' -sameq '.$neededFile,$out);
			unlink($presentFile);
			exec('ffmpeg -i '.$neededFile.' -f image2 -s 320x240 '.$presentFile.'.jpg',$out);
		}
		$this->file = $this->_config['name'].'.'.$format;
	}

	public function getUrl() {
		
		return $this->_config['path'].$this->file;
	}

	public function getThumb() {
		
		return $this->_config['path'].$this->_config['name'].'.jpg';
	}

	public function wipe() {
	
		unlink(PUBLIC_PATH.$this->_config['path'].$this->file);
		unlink(PUBLIC_PATH.$this->_config['path'].$this->_config['name'].'.jpg');
	}
}
