<?php

class Application_Model_MailSender {

	private $_template;
	private $_config;
	private $_mappers = array();

	public function __construct() {
		
		$this->_template = new Zend_View();
		$this->_template->setScriptPath(APPLICATION_PATH.'/views/scripts/email/');
		$this->_config = Zend_Registry::get('config');
		$this->_mappers['Girl'] = new Application_Model_GirlMapper;
		$this->_mappers['User'] = new Application_Model_UserMapper;
	}

	public function sendApplicantData($applicantId) {
		
		$applicant = $this->_mappers['Girl']->fetch($applicantId);
		$applicantUser = $this->_mappers['User']->fetch($applicantId);
		$this->_template->assign('applicant',$applicant);
		$this->_template->assign('photos',$applicant->photos);
		$this->_template->assign('applicantUser',$applicantUser);
		$this->_template->assign('host',$this->_config->webhost);
		$mail = new Zend_Mail('utf-8');
		$bodyText = $this->_template->render('register.phtml');	
		$mail->addTo($this->_config->email->adminAddress);
		$mail->setSubject('Новая претендентка на участие в конкурсе');
		$mail->setBodyHtml($bodyText);
		$mail->send();
	}

	public function sendApproval($applicantId,$password) {
		
		$applicantUser = $this->_mappers['User']->fetch($applicantId);
		$this->_template->assign('user',$applicantUser);
		$this->_template->assign('host',$this->_config->webhost);
		$this->_template->assign('password',$password);
		$mail = new Zend_Mail('utf-8');
		$bodyText = $this->_template->render('approved.phtml');	
		$mail->addTo($applicantUser->email);
		$mail->setSubject('Подтверждение на участие в конкурсе');
		$mail->setBodyHtml($bodyText);
		$mail->send();
	}

	public function sendNewPassword($email,$password) {
		
		$this->_template->assign('host',$this->_config->webhost);
		$this->_template->assign('password',$password);
		$mail = new Zend_Mail('utf-8');
		$bodyText = $this->_template->render('newPassword.phtml');	
		$mail->addTo($email);
		$mail->setSubject('Новый пароль');
		$mail->setBodyHtml($bodyText);
		$mail->send();
	}
}
