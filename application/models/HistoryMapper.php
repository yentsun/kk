<?php

class Application_Model_HistoryMapper extends Whyte_Model_Mapper {

	protected $_namespace = 'History';
	protected $_entityClass = 'Application_Model_History';
	protected $_gateway = null;
	protected $_map = null;
	protected $_history = null;

	protected function _init() {

		$this->_gateway = new Zend_Session_Namespace($this->_namespace);
		$this->_history = $this->loadHistory();
	}

	protected function _setMap() { 

		$this->_map = array();
	}

	protected function _loadFromGateway() {

		if (!isset($this->getGateway()->history))
			$this->getGateway()->history = array();
		$result = $this->getGateway()->history;
		return $result;
	}

	public function loadHistory() {

		return $this->_assembleHistory($this->_loadFromGateway());
	}

	public function recordPresent($type,$value) {
	
		$history = $this->_history;
		foreach ($history as $item) {
			if ($item->type == $type && $item->value == $value) 
				return true;
		}
		return false;
	}

	public function getUnique($type,$qty) {

		$result = array();
		$history = $this->_history;
		ksort($history,SORT_NUMERIC);
		foreach ($history as $item) {
			if ($item->type == $type) 
				$result[] = $item->value;
		}
		return array_slice(array_unique(array_reverse($result)),0,$qty);
	}

	public function getByType($type) {
		
		foreach ($this->_history as $item) {
			if ($item->type == $type) 
				return $item;
		}
		return false;
	}

	public function updateByType($type,$newValue) {
		
		$item = $this->getByType($type);
		if ($item) {
			$item->value = $newValue;
			$this->_history[$item->id] = $item;
			$this->save();
		} else {
			$this->addItem($type,$newValue);
		}
	}

	public function addItem($type,$value) {

		if (!$this->recordPresent($type,$value)) {
			$newItem = array(
				'id'	=>	time(),
				'type'	=>	$type,
				'value'	=>	$value
			);
			$this->_history[$newItem['id']] = $this->_assembleItem($newItem);
			$this->save();
		}
	}
	
	public function reset() {

		$this->_gateway->history = array();
	}

	public function save() {

		$history = $this->_history;
		$disassembledHistory = array();
		foreach ($history as $item) {
			$disassembledHistory[$item->id] = $item->toArray();
		}
		$this->_gateway->history = $disassembledHistory;
	}

	protected function _assembleHistory(array $data) {

		$assembledHistory = array();
		foreach ($data as $id => $disassembledItem) {
			$assembledHistory[$id] = $this->_assembleItem($disassembledItem);
		}
		return $assembledHistory;
	}

	protected function _assembleItem(array $disassembledItem) {

		$assembled = new Application_Model_HistoryItem($disassembledItem);
		return $assembled;
	}
}
