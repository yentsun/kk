<?php

class Application_Model_HistoryItem extends Whyte_Model_Entity {

	protected function _dataPattern() {

		return array(
			'id' => 'Digits',
			'type' => array(), //тип записи истории, например "голосование", "просмотренный товар"
			'value' => array()
		);
	}
}
