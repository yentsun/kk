<?php

class Application_Model_ImageMapper extends Whyte_Model_Mapper {

	protected $_tableName = 'photos';
	protected $_entityClass = 'Application_Model_Image';
	protected $_joinBy = 'user_id';

	protected function _setMap() {

		$this->_map = array(
			'id'		=> 'photo_id',
		    	'parentId'	=> 'user_id',
		    	'file' 		=> 'photo_file',
		    	'status' 	=> 'photo_status',
		    	'description'	=> 'photo_description',
		);
	}

	public function fetchByGirlId($userId,$status=1) {
	
		$select = $this->_gateway
			->select()
			->where($this->_map['parentId'].' = ?',$userId)
			->where($this->_map['status'].' = ?',$status)
			;
		$rows = $this->_gateway->fetchAll($select);
		$items = array();
		foreach ($rows as $row) {
			$items[] = $this->_factory($row);
		}
		return $items;
	}

	public function fetch($photoId) {
	
		$select = $this->_gateway
			->select()
			->where($this->_map['id'].' = ?',$photoId);
		$row = $this->_gateway->fetchRow($select);
		return $this->_factory($row);
	}

	public function add($data) {
		
		$image = new $this->_entityClass($data);
		if ($image->hasErrors())
			return $this->_errorAndRepopulate($image->getErrors(),$data);
		$image->file = $data['file'];
		$image->id = $this->_gateway->insert($this->_toMappedArray($image));
		return $image;
	}

	public function delete($photoId) {
		
		$image = $this->fetch($photoId);
		$image->wipe();
		$this->_gateway->delete($this->_map['id'].' = '.$photoId);
	}
}
