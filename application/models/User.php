<?php

class Application_Model_User extends Whyte_Model_Entity {

	protected function _dataPattern() {

		return array(
		    'id' => array(array('GreaterThan',1),'presence' => 'required'),
		    'email' => array('EmailAddress','presence' => 'required'),
		    'salt' => array('presence' => 'required'),
		    'digest' => array('Hex',array('StringLength',32,32),'presence' => 'required'),
		    'firstName' => array('presence' => 'required'),
		    'lastName' => array(),
		    'registrationDate' => array(array('Date','YYYY-MM-dd'),'presence' => 'required'),
		    'status' => array('presence'=>'required')
		);
	}
	
	public function digest($password) {
		
		$this->digest = md5($this->salt.$password);
	}
}
