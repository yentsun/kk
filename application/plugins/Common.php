<?php

class Application_Plugin_Common extends Zend_Controller_Plugin_Abstract {

	public function preDispatch(Zend_Controller_Request_Abstract $request) {

		$layout = Zend_Layout::getMvcInstance();
		$view = $layout->getView();
		$config = Zend_Registry::get('config');

		$view->headTitle($config->title->general)->setSeparator(' - ');

		//blocks
		$pageMapper = new Application_Model_PageMapper;
		$view->blocks = $pageMapper->fetchBlocks();

		//user
		$usersMapper = new Application_Model_UserMapper;
		$user = $usersMapper->getUser();
		Zend_Registry::set('user',$user);
		$view->authorizedUser = $user;

		//flash messages
		$flashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
		$view->messages = $flashMessenger->getMessages();
	}
}
