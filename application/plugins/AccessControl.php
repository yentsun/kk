<?php

class Application_Plugin_AccessControl extends Zend_Controller_Plugin_Abstract {

	public function preDispatch(Zend_Controller_Request_Abstract $request) {

		$acl = new Zend_Acl();

		if ($this->_response->isException()) {
			$this->getResponse()->setHttpResponseCode(404);
			return;
		}

		//роли
		$acl->addRole(new Zend_Acl_Role('guest'));
		$acl->addRole(new Zend_Acl_Role('model'));
		$acl->addRole(new Zend_Acl_Role('editor'),'model');
		$acl->addRole(new Zend_Acl_Role('admin'),'editor');

		//ресурсы
		$acl->add(new Zend_Acl_Resource('error'));
		$acl->add(new Zend_Acl_Resource('index'));
		$acl->add(new Zend_Acl_Resource('gallery'));
		$acl->add(new Zend_Acl_Resource('girl'));
		$acl->add(new Zend_Acl_Resource('post'));
		$acl->add(new Zend_Acl_Resource('page'));
		$acl->add(new Zend_Acl_Resource('photos'));
		$acl->add(new Zend_Acl_Resource('video'));
		$acl->add(new Zend_Acl_Resource('admin'));
		$acl->add(new Zend_Acl_Resource('user'));
		$acl->add(new Zend_Acl_Resource('ckfinder'));

		//открыто для всех
		$acl->allow(null,array('index','error','page','post','girl','gallery'));

		//права гостя
		$acl->allow('guest','user',array('login','register','new-password'));

		//права участницы
		$acl->allow('model','user',array('logout','personal','update','photos','videos'));
		$acl->allow('model','photos',array('delete','add'));
		$acl->allow('model','video',array('delete','add'));

		//права редактора
		$acl->allow('editor','admin',array('posts','create-post','delete-post','update-post','pages','update-page'));
		$acl->allow('editor','ckfinder',null);

		//права администратора
		$acl->allow('admin',null);

		$user = Zend_Registry::get('user');
		if ($user) {
			$role = $user->status;
		} else {
			$role = 'guest';
		}
		$controller = $request->controller;
		$action = $request->action;
		if ($acl->has($controller)) {
			if (!$acl->isAllowed($role,$controller,$action)) {
				return $request 
					->setControllerName('error') 
					->setActionName('unauthorized') 
				//	->setParam('', 'Access denied.')
					; 
			}
		}
	}
}
