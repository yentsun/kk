$(function(){
	if ($('input:password').length) {
		$('input:password').dPassword({
			duration: 1000
		});
	}
	$('form label,form input').each(function(){
		var name = $(this).attr('name');
		if ($('#required').length) {
			var data = jQuery.parseJSON($('#required').val());
			if (jQuery.inArray(name,data)!==-1) {
				//$(this).addClass('required');
				$('label[for="'+name+'"]').append('<span class="required">*</span>');
			}
		}
		if ($('.error .repopulate').length) {
			var repopulate = jQuery.parseJSON($('.error .repopulate').first().text());
			for (fieldName in repopulate) {
				$('form *[name="'+fieldName+'"]').val(repopulate[fieldName]);
			}
		}
		if ($('.error .data').length) {
			var errorData = jQuery.parseJSON($('.error .data').text());
			if (errorData[name]) {
				$(this).addClass('errorfield');
				var string = '';
				for (keyVar in errorData[name]) {
					string = string + errorData[name][keyVar]+' ';
				}
				$('label[for="'+name+'"]').append('<br><small style="color:red">'+string+'</small>');
			//console.debug(string);
			}
		}
	});
 	if ($('textarea.rich').length) {
 		/*$('textarea.rich').tinymce({
                	script_url: '/js/tiny_mce/tiny_mce.js',
			theme: 'advanced',
			theme_advanced_toolbar_align: 'left',
			theme_advanced_toolbar_location: 'top'
		});*/
		$('textarea.rich').ckeditor({
			toolbar_Full: [
		 		{ name: 'tech',      items : [ 'Source' ] },
				{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		 		{ name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		 		{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		 		{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
		 		{ name: 'insert',      items : [ 'Image','Flash','Table' ] },
		 	],
			filebrowserBrowseUrl : '/js/ckfinder/ckfinder.html',
			filebrowserImageBrowseUrl : '/js/ckfinder/ckfinder.html?type=Images',
			filebrowserFlashBrowseUrl : '/js/ckfinder/ckfinder.html?type=Flash',
			filebrowserUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
			filebrowserImageUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
			filebrowserFlashUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
		 });
	}
});
