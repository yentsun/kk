$(function(){
	$('.row .item a,.gallery .item a').fancybox({
		'autoDimensions':	true,
		'width'		:	900,
		'height'	:	600,
		'padding'	:	0,
		'showNavArrows'	: 	false
	});
	$('#thumbs .thumb').live('click',function(e){
		e.preventDefault();
		var id = $(this).attr('id').replace('th','');
		$('#big img').fadeOut('fast');
		$('#big'+id).fadeIn();
	});
	$('#side .vote').live('click',function(){
		var vote = $(this).text();
		var id = $(this).attr('rel');
		$.post('/girl/vote',{id:id,rating:vote});
		var rating = parseInt($('#rating span').text());
		var newRating = rating+parseInt(vote);
		$('#rating span').text(newRating);
		$('#votes').fadeOut('slow');
	});
})
