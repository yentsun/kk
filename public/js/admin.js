$(function(){
	$('.row .item a').fancybox({
		'autoDimensions':	true,
		'width'		:	900,
		'height'	:	600,
		'padding'	:	0
	});
	$('.star').click(function(){
		var id = $(this).parents('tr').attr('id').replace('girl','');
		$.post('/girl/select',{id:id});
		if ($(this).hasClass('gold'))
			$(this).removeClass('gold');
		else
			$(this).addClass('gold');
	});
	$('.button.delete').click(function(){
		return confirm('Подтвердите удаление!');	
	});
})
