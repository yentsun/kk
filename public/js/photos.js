$(function(){
	$('.photo').draggable({
		start: function(event,ui){
			ui.helper.removeMe = true;
		},
		stop: function(event,ui){
			if (ui.helper.removeMe) {
				var id = ui.helper.attr('id').replace('photo','');
				ui.helper.remove();
				$.get('/photos/delete/id/'+id);
			}
		},
		revert: 'valid'
	});
	$('.photos').droppable({
		drop: function(event,ui){
			alert('moved');
			ui.helper.removeMe = false;
		}
	});

	//загрузка фотографий
	$('input.file').change(function() {
		$(this).parents('form').submit();
	});
	$('form.upload').ajaxForm({
		dataType : 'xml',
		beforeSubmit: loadingPhoto,
		success:   newPhotoSuccess
	});
})

function loadingPhoto(data,form) {
	$(form).before('<div class="photo loading"></div>');
}

function newPhotoSuccess(responseXML,statusText,form,form) {
	var status = $('status',responseXML).text();
	var thumb = $('thumb',responseXML).text();
	var big = $('big',responseXML).text();
	var id = $('id',responseXML).text();
	var thumbHtml = '<a class="photo" id="photo'+id+'" href="'+big+'" style="background-image:url('+thumb+')"></a>';
	if (status=='ok') {
		$(form).parents('.photos').find('.loading').remove();
		$(thumbHtml).hide().insertBefore(form).fadeIn('slow');
	} else {
		var errorText = $('text',responseXML).text();
		$(form).parents('.photos').find('.loading').remove();
		alert('Ошибка :(');
	}
}
