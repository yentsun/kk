$(function(){
	$('a.video').flowplayer("/js/flowplayer-3.2.7.swf", {
	});
	$('.video').draggable({
		start: function(event,ui){
			ui.helper.removeMe = true;
		},
		stop: function(event,ui){
			if (ui.helper.removeMe) {
				var id = ui.helper.attr('id').replace('video','');
				ui.helper.remove();
				$.get('/video/delete/id/'+id);
			}
		},
		revert: 'valid'
	});
	$('#paper').droppable({
		drop: function(event,ui){
			ui.helper.removeMe = false;
		}
	});
})
