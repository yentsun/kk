SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

use `konkursi_db`;

-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `users` ;

CREATE  TABLE IF NOT EXISTS `users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор' ,
  `user_email` VARCHAR(45) NOT NULL ,
  `user_salt` CHAR(50) NOT NULL ,
  `user_digest` CHAR(32) NOT NULL ,
  `user_fname` VARCHAR(45) NULL DEFAULT NULL ,
  `user_lname` VARCHAR(45) NULL DEFAULT NULL ,
  `user_registered_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `user_status` ENUM('applicant','model','admin') NOT NULL DEFAULT 'applicant' ,
  PRIMARY KEY (`user_id`) ,
  UNIQUE INDEX `user_email_UNIQUE` (`user_email` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 73
DEFAULT CHARACTER SET = utf8
COMMENT = 'пользователи - участницы, администраторы и тд' ;


-- -----------------------------------------------------
-- Table `girls_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `girls_data` ;

CREATE  TABLE IF NOT EXISTS `girls_data` (
  `data_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `girl_birthdate` DATE NULL DEFAULT NULL ,
  `girl_phone` CHAR(11) NULL DEFAULT NULL ,
  `user_id` INT(11) NOT NULL ,
  `girl_height` INT(11) NULL DEFAULT NULL ,
  `girl_weight` INT(11) NULL DEFAULT NULL ,
  `girl_measures` VARCHAR(45) NULL DEFAULT NULL ,
  `girl_hair` VARCHAR(45) NULL DEFAULT NULL ,
  `girl_city` VARCHAR(45) NULL DEFAULT NULL ,
  `girl_rating` INT(11) NULL DEFAULT '0' ,
  `girl_votes` INT(11) NULL DEFAULT '0' ,
  `girl_private_info` TEXT NULL DEFAULT NULL ,
  `girl_selected` TINYINT(1) NULL DEFAULT '0' ,
  PRIMARY KEY (`data_id`) ,
  INDEX `fk_users_data_user` (`user_id` ASC) ,
  CONSTRAINT `fk_users_data_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `users` (`user_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 69
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `pages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pages` ;

CREATE  TABLE IF NOT EXISTS `pages` (
  `page_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `page_title` VARCHAR(45) NOT NULL ,
  `page_slug` VARCHAR(45) NOT NULL ,
  `page_body` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`page_id`) ,
  INDEX `slug` (`page_slug` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `photos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `photos` ;

CREATE  TABLE IF NOT EXISTS `photos` (
  `photo_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `photo_description` TINYTEXT NULL DEFAULT NULL ,
  `photo_file` VARCHAR(45) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  PRIMARY KEY (`photo_id`) ,
  INDEX `fk_photos_user` (`user_id` ASC) ,
  CONSTRAINT `fk_photos_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `users` (`user_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 171
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `posts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `posts` ;

CREATE  TABLE IF NOT EXISTS `posts` (
  `post_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `post_title` VARCHAR(45) NULL DEFAULT NULL ,
  `post_body` TEXT NULL DEFAULT NULL ,
  `post_created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`post_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `videos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `videos` ;

CREATE  TABLE IF NOT EXISTS `videos` (
  `video_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `video_title` VARCHAR(45) NULL DEFAULT NULL ,
  `video_file` VARCHAR(45) NULL DEFAULT NULL ,
  `video_youtube_id` VARCHAR(45) NULL DEFAULT NULL ,
  `video_description` TINYTEXT NULL DEFAULT NULL ,
  `user_id` INT(11) NULL DEFAULT NULL COMMENT 'идентификатор пользователя-владельца' ,
  PRIMARY KEY (`video_id`) ,
  INDEX `fk_videos_user` (`user_id` ASC) ,
  CONSTRAINT `fk_videos_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `users` (`user_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
